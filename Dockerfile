FROM centos:7

RUN yum update -y && yum install python3 puthon3-pip -y
RUN mkdir /python_api
COPY requirements.txt /python_api/requirements.txt
RUN pip3 install -r /python_api/requirements.txt
COPY python-api.py /python_api/python-api.py
CMD ["python3", "/python_api/python-api.py"]
